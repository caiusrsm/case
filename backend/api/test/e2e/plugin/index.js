// https://docs.cypress.io/guides/guides/plugins-guide.html

// if you need a custom webpack configuration you can uncomment the following import
// and then use the `file:preprocessor` event
// as explained in the cypress docs
// https://docs.cypress.io/api/plugins/preprocessors-api.html#Examples

/* eslint-disable import/no-extraneous-dependencies, global-require, arrow-body-style */
// const webpack = require("@cypress/webpack-preprocessor");

module.exports = (on, config) => {
  // on("file:preprocessor", webpack({
  //  watchOptions: {},
  //  webpackOptions: require("@vue/cli-service/webpack.config")
  // }));

  return Object.assign({}, config, {
    fixturesFolder: "test/e2e/fixture",
    integrationFolder: "test/e2e/spec",
    screenshotsFolder: "cypress/screenshot",
    supportFile: "test/e2e/support/index.js",
    videosFolder: "cypress/video"
  });
};
