// IMPORT - ~/node_modules
const dotenv_flow = require("dotenv-flow");

dotenv_flow.config({
  default_node_env: process.env.NODE_ENV_DEFAULT || "alpha",
  node_env: process.env.NODE_ENV,
  path: process.env.NODE_ENV_PATH || "."
});

/* ARGON2 */
if (!process.env.ARGON2_HASH_LENGTH) {
  console.log(
    `ENVIRONMENT INFO: ARGON2_HASH_LENGTH is not defined, initializing it with value "64".`
  );

  process.env.ARGON2_HASH_LENGTH = "64";
}

if (!process.env.ARGON2_MEMORY_COST) {
  console.log(
    `ENVIRONMENT INFO: ARGON2_MEMORY_COST is not defined, initializing it with value "65536".`
  );

  process.env.ARGON2_MEMORY_COST = "65536";
}

if (!process.env.ARGON2_PARALLELISM) {
  console.log(
    `ENVIRONMENT INFO: ARGON2_PARALLELISM is not defined, initializing it with value "4".`
  );

  process.env.ARGON2_PARALLELISM = "4";
}

if (!process.env.ARGON2_TIME_COST) {
  console.log(
    `ENVIRONMENT INFO: ARGON2_TIME_COST is not defined, initializing it with value "4".`
  );

  process.env.ARGON2_TIME_COST = "4";
}

if (!process.env.ARGON2_TYPE) {
  console.log(
    `ENVIRONMENT INFO: ARGON2_TYPE is not defined, initializing it with value "1".`
  );

  process.env.ARGON2_TYPE = "1";
}

/* DATABASE */
if (!process.env.DATABASE_URI) {
  throw Error("EnvironmentError: DATABASE_URI is not defined.");
}

if (!process.env.DATABASE_POOL_ACQUIRE) {
  console.log(
    `ENVIRONMENT INFO: DATABASE_POOL_ACQUIRE is not defined, initializing it with value "30000".`
  );

  process.env.DATABASE_POOL_ACQUIRE = "30000";
}

if (!process.env.DATABASE_POOL_IDLE) {
  console.log(
    `ENVIRONMENT INFO: DATABASE_POOL_IDLE is not defined, initializing it with value "10000".`
  );

  process.env.DATABASE_POOL_IDLE = "10000";
}

if (!process.env.DATABASE_POOL_MAX) {
  console.log(
    `ENVIRONMENT INFO: DATABASE_POOL_MAX is not defined, initializing it with value "5".`
  );

  process.env.DATABASE_POOL_MAX = "5";
}

if (!process.env.DATABASE_POOL_MIN) {
  console.log(
    `ENVIRONMENT INFO: DATABASE_POOL_MIN is not defined, initializing it with value "0".`
  );

  process.env.DATABASE_POOL_MIN = "0";
}

if (!process.env.DATABASE_TIMEZONE) {
  console.log(
    `ENVIRONMENT INFO: DATABASE_TIMEZONE is not defined, initializing it with value "Etc/GMT0".`
  );

  process.env.DATABASE_TIMEZONE = "Etc/GMT0";
}

/* JWT */
if (!process.env.JWT_EXPIRES_IN) {
  console.log(
    `ENVIRONMENT INFO: JWT_EXPIRES_IN is not defined, initializing it with value "24h".`
  );

  process.env.JWT_EXPIRES_IN = "24h";
}

if (!process.env.JWT_ALGORITHM) {
  throw new Error("EnvironmentError: JWT_ALGORITHM is not defined.");
}

if (!process.env.JWT_ISSUER) {
  throw new Error("EnvironmentError: JWT_ISSUER is not defined.");
}

if (!process.env.JWT_PRIVATE_KEY) {
  throw new Error("EnvironmentError: JWT_PRIVATE_KEY is not defined.");
}

if (!process.env.JWT_PUBLIC_KEY) {
  throw new Error("EnvironmentError: JWT_PUBLIC_KEY is not defined.");
}

/* LOGGER */
if (!process.env.LOGGER_FORMAT_ALERT) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_ALERT is not defined, initializing it with value "dim black whiteBG".`
  );

  process.env.LOGGER_FORMAT_ALERT = "dim black whiteBG";
}

if (!process.env.LOGGER_FORMAT_CRIT) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_CRIT is not defined, initializing it with value "underline magenta whiteBG".`
  );

  process.env.LOGGER_FORMAT_CRIT = "underline magenta whiteBG";
}

if (!process.env.LOGGER_FORMAT_DEBUG) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_DEBUG is not defined, initializing it with value "blue whiteBG".`
  );

  process.env.LOGGER_FORMAT_DEBUG = "blue whiteBG";
}

if (!process.env.LOGGER_FORMAT_EMERG) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_EMERG is not defined, initializing it with value "dim blue whiteBG".`
  );

  process.env.LOGGER_FORMAT_EMERG = "dim blue whiteBG";
}

if (!process.env.LOGGER_FORMAT_ERROR) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_ERROR is not defined, initializing it with value "bold red whiteBG".`
  );

  process.env.LOGGER_FORMAT_ERROR = "bold red whiteBG";
}

if (!process.env.LOGGER_FORMAT_INFO) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_INFO is not defined, initializing it with value "green whiteBG".`
  );

  process.env.LOGGER_FORMAT_INFO = "green whiteBG";
}

if (!process.env.LOGGER_FORMAT_NOTICE) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_NOTICE is not defined, initializing it with value "strikethrough grey whiteBG".`
  );

  process.env.LOGGER_FORMAT_NOTICE = "strikethrough grey whiteBG";
}

if (!process.env.LOGGER_FORMAT_WARNING) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_FORMAT_WARNING is not defined, initializing it with value "italic yellow whiteBG".`
  );

  process.env.LOGGER_FORMAT_WARNING = "italic yellow whiteBG";
}

if (!process.env.LOGGER_INDEX_ALERT) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_ALERT is not defined, initializing it with value "1".`
  );

  process.env.LOGGER_INDEX_ALERT = "1";
}

if (!process.env.LOGGER_INDEX_CRIT) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_CRIT is not defined, initializing it with value "2".`
  );

  process.env.LOGGER_INDEX_CRIT = "2";
}

if (!process.env.LOGGER_INDEX_DEBUG) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_DEBUG is not defined, initializing it with value "7".`
  );

  process.env.LOGGER_INDEX_DEBUG = "7";
}

if (!process.env.LOGGER_INDEX_EMERG) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_EMERG is not defined, initializing it with value "0".`
  );

  process.env.LOGGER_INDEX_EMERG = "0";
}

if (!process.env.LOGGER_INDEX_ERROR) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_ERROR is not defined, initializing it with value "3".`
  );

  process.env.LOGGER_INDEX_ERROR = "3";
}

if (!process.env.LOGGER_INDEX_INFO) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_INFO is not defined, initializing it with value "6".`
  );

  process.env.LOGGER_INDEX_INFO = "6";
}

if (!process.env.LOGGER_INDEX_NOTICE) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_NOTICE is not defined, initializing it with value "5".`
  );

  process.env.LOGGER_INDEX_NOTICE = "5";
}

if (!process.env.LOGGER_INDEX_WARNING) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_INDEX_WARNING is not defined, initializing it with value "4".`
  );

  process.env.LOGGER_INDEX_WARNING = "4";
}

if (!process.env.LOGGER_LEVEL_ACCESS) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_LEVEL_ACCESS is not defined, initializing it with value "alert".`
  );

  process.env.LOGGER_LEVEL_ACCESS = "alert";
}

if (!process.env.LOGGER_LEVEL_CONSOLE) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_LEVEL_CONSOLE is not defined, initializing it with value "debug".`
  );

  process.env.LOGGER_LEVEL_CONSOLE = "debug";
}

if (!process.env.LOGGER_LEVEL_ERROR) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_LEVEL_ERROR is not defined, initializing it with value "error".`
  );

  process.env.LOGGER_LEVEL_ERROR = "error";
}

if (!process.env.LOGGER_MESSAGE) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_MESSAGE is not defined, initializing it with value "COMBINED".`
  );

  process.env.LOGGER_MESSAGE = "COMBINED";
}

if (!process.env.LOGGER_PATH) {
  console.log(
    `ENVIRONMENT INFO: LOGGER_PATH is not defined, initializing it with value "log".`
  );

  process.env.LOGGER_PATH = "log";
}

/* PLAYGROUND */
if (process.env.NODE_ENV !== "production") {
  if (!process.env.PLAYGROUND) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND is not defined, initializing it with value "true".`
    );

    process.env.PLAYGROUND = "true";
  }

  if (!process.env.PLAYGROUND_ENDPOINT) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_ENDPOINT is not defined, initializing it with value "/graphql".`
    );

    process.env.PLAYGROUND_ENDPOINT = "/graphql";
  }

  if (!process.env.PLAYGROUND_ENDPOINT_SUBSCRIPTION) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_ENDPOINT_SUBSCRIPTION is not defined, initializing it with value "/graphql/subscription".`
    );

    process.env.PLAYGROUND_ENDPOINT_SUBSCRIPTION = "/graphql/subscription";
  }

  if (!process.env.PLAYGROUND_SETTINGS_EDITOR_CURSOR_SHAPE) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_EDITOR_CURSOR_SHAPE is not defined, initializing it with value "underline".`
    );

    process.env.PLAYGROUND_SETTINGS_EDITOR_CURSOR_SHAPE = "underline";
  }

  if (!process.env.PLAYGROUND_SETTINGS_EDITOR_FONT_FAMILY) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_EDITOR_FONT_FAMILY is not defined, initializing it with value "'Source Code Pro', 'Consolas', 'Inconsolata', 'Droid Sans Mono', 'Monaco', monospace".`
    );

    process.env.PLAYGROUND_SETTINGS_EDITOR_FONT_FAMILY = `"Source Code Pro", "Consolas", "Inconsolata", "Droid Sans Mono", "Monaco", monospace`;
  }

  if (!process.env.PLAYGROUND_SETTINGS_EDITOR_FONT_SIZE) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_EDITOR_FONT_SIZE is not defined, initializing it with value "14".`
    );

    process.env.PLAYGROUND_SETTINGS_EDITOR_FONT_SIZE = "14";
  }

  if (!process.env.PLAYGROUND_SETTINGS_EDITOR_REUSE_HEADERS) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_EDITOR_REUSE_HEADERS is not defined, initializing it with value "true".`
    );

    process.env.PLAYGROUND_SETTINGS_EDITOR_REUSE_HEADERS = "true";
  }

  if (!process.env.PLAYGROUND_SETTINGS_EDITOR_THEME) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_EDITOR_THEME is not defined, initializing it with value "dark".`
    );

    process.env.PLAYGROUND_SETTINGS_EDITOR_THEME = "dark";
  }

  if (!process.env.PLAYGROUND_SETTINGS_GENERAL_BETA_UPDATES) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_GENERAL_BETA_UPDATES is not defined, initializing it with value "false".`
    );

    process.env.PLAYGROUND_SETTINGS_GENERAL_BETA_UPDATES = "false";
  }

  if (!process.env.PLAYGROUND_SETTINGS_REQUEST_CREDENTIALS) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_REQUEST_CREDENTIALS is not defined, initializing it with value "omit".`
    );

    process.env.PLAYGROUND_SETTINGS_REQUEST_CREDENTIALS = "omit";
  }

  if (!process.env.PLAYGROUND_SETTINGS_TRACING_HIDE_TRACING_RESPONSE) {
    console.log(
      `ENVIRONMENT INFO: PLAYGROUND_SETTINGS_TRACING_HIDE_TRACING_RESPONSE is not defined, initializing it with value "true".`
    );

    process.env.PLAYGROUND_SETTINGS_TRACING_HIDE_TRACING_RESPONSE = "true";
  }
}

/* ROOT USER */
if (!process.env.ROOT_USER_NAME) {
  console.log(
    `ENVIRONMENT INFO: ROOT_USER_NAME is not defined, initializing it with value "Root User".`
  );

  process.env.ROOT_USER_NAME = "Root User";
}

if (!process.env.ROOT_USER_PERMISSION_GROUP) {
  console.log(
    `ENVIRONMENT INFO: ROOT_USER_PERMISSION_GROUP is not defined, initializing it with value "Root".`
  );

  process.env.ROOT_USER_PERMISSION_GROUP = "Root";
}

if (!process.env.ROOT_USER_PERMISSION_GROUP_METHOD) {
  console.log(
    `ENVIRONMENT INFO: ROOT_USER_PERMISSION_GROUP_METHOD is not defined, initializing it with value ".*".`
  );

  process.env.ROOT_USER_PERMISSION_GROUP_METHOD = ".*";
}

if (!process.env.ROOT_USER_PERMISSION_GROUP_PATH) {
  console.log(
    `ENVIRONMENT INFO: ROOT_USER_PERMISSION_GROUP_PATH is not defined, initializing it with value ".*".`
  );

  process.env.ROOT_USER_PERMISSION_GROUP_PATH = ".*";
}

if (!process.env.ROOT_USER_USERNAME) {
  console.log(
    `ENVIRONMENT INFO: ROOT_USER_USERNAME is not defined, initializing it with value "root".`
  );

  process.env.ROOT_USER_USERNAME = "root";
}

if (!process.env.ROOT_USER_EMAIL) {
  throw new Error("EnvironmentError: ROOT_USER_EMAIL is not defined.");
}

if (!process.env.ROOT_USER_PASSWORD) {
  throw new Error("EnvironmentError: ROOT_USER_PASSWORD is not defined.");
}

/* SYSTEM */
if (!process.env.SYSTEM_HOST) {
  console.log(
    `ENVIRONMENT INFO: SYSTEM_HOST is not defined, initializing it with value "0.0.0.0".`
  );

  process.env.SYSTEM_HOST = "0.0.0.0";
}

if (!process.env.SYSTEM_PORT) {
  console.log(
    `ENVIRONMENT INFO: SYSTEM_PORT is not defined, initializing it with value "3000".`
  );

  process.env.SYSTEM_PORT = "3000";
}

if (!process.env.SYSTEM_NAME) {
  throw new Error("EnvironmentError: SYSTEM_NAME is not defined.");
}

/* VUE */
if (!process.env.VUE_CLI_BABEL_TRANSPILE_MODULES) {
  console.log(
    `ENVIRONMENT INFO: VUE_CLI_BABEL_TRANSPILE_MODULES is not defined, initializing it with value "true".`
  );

  process.env.VUE_CLI_BABEL_TRANSPILE_MODULES = "true";
}
