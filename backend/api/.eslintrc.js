module.exports = {
  env: {
    node: true
  },
  extends: [
    "plugin:@typescript-eslint/all",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:import/typescript",
    "plugin:vue/recommended",
    "@vue/prettier",
    "@vue/prettier/recommended",
    "@vue/typescript/recommended"
  ],
  plugins: [
    "const-case"
  ],
  overrides: [
    {
      files: ["**/*.js", "**/*.jsx"],
      parserOptions: {
        parser: "babel-eslint"
      },
      rules: {
        "@typescript-eslint/await-thenable": "off",
        "@typescript-eslint/explicit-member-accessibility": "off",
        "@typescript-eslint/prefer-includes": "off",
        "@typescript-eslint/prefer-nullish-coalescing": "off",
        "@typescript-eslint/prefer-readonly": "off",
        "@typescript-eslint/prefer-regexp-exec": "off",
        "@typescript-eslint/prefer-string-starts-ends-with": "off",
        "@typescript-eslint/promise-function-async": "off",
        "@typescript-eslint/no-floating-promises": "off",
        "@typescript-eslint/no-implied-eval": "off",
        "@typescript-eslint/no-misused-promises": "off",
        "@typescript-eslint/no-throw-literal": "off",
        "@typescript-eslint/no-unnecessary-boolean-literal-compare": "off",
        "@typescript-eslint/no-unnecessary-condition": "off",
        "@typescript-eslint/no-unnecessary-qualifier": "off",
        "@typescript-eslint/no-unnecessary-type-arguments": "off",
        "@typescript-eslint/no-unnecessary-type-assertion": "off",
        "@typescript-eslint/no-unused-vars-experimental": "off",
        "@typescript-eslint/require-array-sort-compare": "off",
        "@typescript-eslint/restrict-plus-operands": "off",
        "@typescript-eslint/return-await": "off",
        "@typescript-eslint/strict-boolean-expressions": "off",
        "@typescript-eslint/switch-exhaustiveness-check": "off",
        "@typescript-eslint/unbound-method": "off"
      }
    },
    {
      files: ["**/*.ts", "**/*.tsx"],
      parserOptions: {
        parser: "@typescript-eslint/parser",
        project: ["tsconfig.json", "test/**/tsconfig.json"]
      }
    },
    {
      env: {
        jest: true
      },
      files: [
        "**/__test__/*.{j,t}s?(x)",
        "**/test/unit/**/*.spec.{j,t}s?(x)"
      ]
    },
    {
      files: ["babel.config.js", "dotenv-flow.config.js", "jest.config.js"],
      rules: {
        "@typescript-eslint/no-require-imports": "off",
        "@typescript-eslint/no-var-requires": "off"
      }
    }
  ],
  root: true,
  rules: {
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/indent": "off",
    "@typescript-eslint/interface-name-prefix": ["error", { prefixWithI: "always" }],
    "@typescript-eslint/member-ordering": [
      "error",
      {
        default: [
          "signature",
          "public-constructor",
          "protected-constructor",
          "private-constructor",
          "public-field",
          "protected-field",
          "private-field",
          "public-method",
          "protected-method",
          "private-method"
        ]
      }
    ],
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-magic-numbers": [
      "warn",
      {
        ignoreEnums: true,
        ignoreNumericLiteralTypes: true,
        ignoreReadonlyClassProperties: true
      }
    ],
    "@typescript-eslint/no-parameter-properties": "off",
    "@typescript-eslint/no-type-alias": "off",
    "@typescript-eslint/quotes": ["error", "double", { allowTemplateLiterals: true, avoidEscape: true }],
    "@typescript-eslint/restrict-template-expressions": "off",
    "@typescript-eslint/require-await": "off",
    "@typescript-eslint/space-before-function-paren": "off",
    "@typescript-eslint/strict-boolean-expressions": "off",
    "@typescript-eslint/typedef": "off",
    "const-case/uppercase": "error",
    "import/order": [
      "error",
      {
        alphabetize: {
          caseInsensitive: true,
          order: "asc"
        }
      }
    ],
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-shadow": ["error", { builtinGlobals: true }],
    "sort-imports": ["error", { ignoreDeclarationSort: true }],
    "sort-keys": ["error", "asc", { natural: true }],
    "sort-vars": "error"
  },
  "settings": {
    "import/ignore": ["lodash", "lodash/fp", "rxjs", "rxjs/operators"],
    "import/parser": {
      "@typescript-eslint/parser": [".ts", ".tsx"],
      "babel-eslint": [".js", ".jsx"],
      "vue-eslint-parser": [".vue"]
    },
    "import/resolver": {
      "typescript": {
        "alwaysTryTypes": true
      }
    }
  }
};
