// IMPORT - ~/node_modules
import {
  RenderPageOptions,
  renderPlaygroundPage
} from "graphql-playground-html/dist/render-playground-page";
import { iif, of } from "rxjs";
import { mergeMapTo } from "rxjs/operators";

// IMPORT - @/effect
import { TEffect } from "@/effect";

// IMPORT - @/helper
import not_found$ from "@/helper/error/http/not_found$";

// EXPORT - Factory
const factory: TEffect<{
  playground: RenderPageOptions;
  condition: boolean;
}> = option => req$ =>
  req$.pipe(
    mergeMapTo(
      iif(
        () => option.condition,
        of({
          body: renderPlaygroundPage(option.playground),
          headers: { "content-type": "text/html" }
        }),
        not_found$()()
      )
    )
  );

export default factory;
