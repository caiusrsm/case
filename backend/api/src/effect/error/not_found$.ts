// IMPORT - ~/node_modules
import { mergeMapTo } from "rxjs/operators";

// IMPORT - @/effect
import { TEffect } from "@/effect";

// IMPORT - @/helper
import not_found$ from "@/helper/error/http/not_found$";

// EXPORT - Factory
const factory: TEffect<void, "defaulted"> = () => req$ =>
  req$.pipe(mergeMapTo(not_found$()()));

export default factory;
