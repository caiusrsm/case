// IMPORT - ~/node_modules
import { HttpRequest } from "@marblejs/core";
import { GraphQLSchema, graphql } from "graphql";
import { assign, get } from "lodash/fp";
import { from } from "rxjs";
import { map, mergeMap } from "rxjs/operators";

// IMPORT - @/effect
import { TEffect } from "@/effect";

// EXPORT - Interface
export interface IGraphQL {
  context?: {};
  schema: GraphQLSchema;
  path?: string;
}

// EXPORT - Factory
const factory: TEffect<IGraphQL> = option => req$ => {
  const graphql_option = assign<IGraphQL>(option)({
    context: {},
    path: "body"
  } as { context: {}; path: string });

  return req$.pipe(
    mergeMap(req => {
      const input = get(graphql_option.path)(req);

      return from(
        graphql(
          graphql_option.schema,
          input.query,
          undefined,
          assign(graphql_option.context)({ req } as { req: HttpRequest }),
          input.variables,
          input.operationName
        )
      );
    }),
    map(response => ({ body: response }))
  );
};

export default factory;
