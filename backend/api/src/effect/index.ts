// IMPORT - ~/node_modules
import { HttpEffect } from "@marblejs/core";

// EXPORT - Type
export type TEffect<
  T,
  U extends "defaulted" | null = null
> = U extends "defaulted"
  ? (option?: T) => HttpEffect
  : (option: T) => HttpEffect;

// EXPORT - Sub-Module
import * as error from "@/effect/error";
import * as graphql$ from "@/effect/graphql$";
import * as playground$ from "@/effect/playground$";

export { error, graphql$, playground$ };
