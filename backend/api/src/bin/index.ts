// IMPORT - ~/node_modules
import "reflect-metadata";
import { createServer, defaultError$, httpListener } from "@marblejs/core";
import { bodyParser$ } from "@marblejs/middleware-body";
import { cors$ } from "@marblejs/middleware-cors";
import { Options } from "argon2";
import { concat, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";

// IMPORT - @/event
import start$ from "@/event/listening/start$";

// IMPORT - @/middleware
import logger$ from "@/middleware/logger$";

// IMPORT - @/route
import not_found$ from "@/route/error/not_found$";
import graphql$ from "@/route/graphql$";
import playground$ from "@/route/playground$";

// IMPORT - @/setting
import * as setting from "@/setting";

// --> START SERVER <-- //
of(true)
  .pipe(
    mergeMap(() =>
      setting.playground$
        .default()()
        .pipe(map(playground => ({ playground })))
    ),
    mergeMap(settings =>
      setting.winston$
        .default()()
        .pipe(map(logger => ({ logger, ...settings })))
    ),
    mergeMap(settings =>
      setting.sequelize$
        .default(settings.logger)()
        .pipe(map(sequelize => ({ sequelize, ...settings })))
    ),
    mergeMap(settings =>
      setting.model$
        .default()()
        .pipe(map(() => settings))
    ),
    mergeMap(settings =>
      setting.argon2$
        .default()()
        .pipe(map(argon2 => ({ argon2, ...settings })))
    ),
    mergeMap(settings =>
      setting.graphql$
        .default({
          sign_in: { argon2: settings.argon2 },
          user: { argon2: { ...settings.argon2 } as Options & { raw?: false } }
        })()
        .pipe(map(graphql => ({ graphql, ...settings })))
    ),
    mergeMap(settings =>
      setting.schema$
        .default(settings.graphql)()
        .pipe(map(schema => ({ schema, ...settings })))
    ),
    map(({ logger, playground, schema }) =>
      createServer({
        event$: (...args) => start$()(...args),
        hostname: process.env.SYSTEM_HOST as string,
        httpListener: httpListener({
          effects: [graphql$(schema), playground$(playground), not_found$()],
          error$: (req$, res, meta) =>
            concat(
              defaultError$(req$, res, meta),
              logger$({ level: process.env.LOGGER_LEVEL_ERROR, logger })(
                req$,
                res,
                meta
              )
            ),
          middlewares: [
            cors$({
              allowHeaders: "*",
              methods: ["GET", "POST", "OPTIONS"],
              origin: "*"
            }),
            bodyParser$(),
            logger$({ logger })
          ]
        }),
        port: (process.env.SYSTEM_PORT as unknown) as number
      })
    )
  )
  .subscribe(server => server.run());
