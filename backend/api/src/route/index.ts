// IMPORT - ~/node_modules
import { RouteEffect, RouteEffectGroup } from "@marblejs/core";

// EXPORT - Type
export type TRoute<
  T,
  U extends "defaulted" | null = null
> = U extends "defaulted"
  ? (option?: T) => RouteEffect | RouteEffectGroup
  : (option: T) => RouteEffect | RouteEffectGroup;

// EXPORT - Sub-Module
import * as error from "@/route/error";
import * as graphql$ from "@/route/graphql$";
import * as playground$ from "@/route/playground$";

export { error, graphql$, playground$ };
