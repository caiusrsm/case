// IMPORT - ~/node_modules
import { combineRoutes, r } from "@marblejs/core";
import { RenderPageOptions } from "graphql-playground-html/dist/render-playground-page";

// IMPORT - @/effect
import playground$ from "@/effect/playground$";

// IMPORT - @/route
import { TRoute } from "@/route";

// EXPORT - Factory
const factory: TRoute<RenderPageOptions> = playground =>
  combineRoutes("/playground", [
    r.pipe(
      r.matchPath("/"),
      r.matchType("GET"),
      r.useEffect(
        playground$({
          condition: (process.env.PLAYGROUND as unknown) as boolean,
          playground
        })
      )
    )
  ]);

export default factory;
