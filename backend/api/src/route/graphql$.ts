// IMPORT - ~/node_modules
import { combineRoutes, r } from "@marblejs/core";
import { GraphQLSchema } from "graphql";

// IMPORT - @/effect
import graphql$ from "@/effect/graphql$";

// IMPORT - @/route
import { TRoute } from "@/route";

// EXPORT - Factory
const factory: TRoute<GraphQLSchema> = schema =>
  combineRoutes("/graphql", [
    r.pipe(
      r.matchPath("/"),
      r.matchType("GET"),
      r.useEffect(graphql$({ path: "query", schema }))
    ),
    r.pipe(
      r.matchPath("/"),
      r.matchType("POST"),
      r.useEffect(graphql$({ schema }))
    )
  ]);

export default factory;
