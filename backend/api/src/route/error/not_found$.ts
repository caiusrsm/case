// IMPORT - ~/node_modules
import { r } from "@marblejs/core";

// IMPORT - @/effect
import not_found$ from "@/effect/error/not_found$";

// IMPORT - @/route
import { TRoute } from "@/route";

// EXPORT - Factory
const factory: TRoute<void, "defaulted"> = () =>
  r.pipe(r.matchPath("*"), r.matchType("*"), r.useEffect(not_found$()));

export default factory;
