// IMPORT
import { readFileSync } from "fs";
import { resolve } from "path";

// IMPORT - node_modules
import { Options, verify } from "argon2";
import { Validator } from "class-validator";
import { signAsync } from "jsonwebtoken-promisified";
import { assign } from "lodash/fp";
import { from } from "rxjs";
import { Op } from "sequelize";
import { Args, Query, Resolver, buildSchema } from "type-graphql";
import { Container, Inject, Service } from "typedi";

// IMPORT - @/graphql
import { TGraphQL } from "@/graphql";
import { SignInArgs } from "@/graphql/sign_in$/args";
import { SignInObject } from "@/graphql/sign_in$/object";

// IMPORT - @/helper
import { account_missing$, incorrect_sign_in$ } from "@/helper/error";

// IMPORT - @/model
import User from "@/model/User";

// EXPORT - Interface
export interface IOption {
  argon2: Options;
  jwt_algorithm?: string;
  jwt_expires_in?: string;
  jwt_issuer?: string;
  jwt_private_key?: string;
}

// EXPORT - Class
@Service()
@Resolver(SignInObject)
export class SignInResolver {
  @Inject("argon2")
  private readonly argon2!: Options;

  @Inject("jwt_algorithm")
  private readonly jwt_algorithm!: string;

  @Inject("jwt_expires_in")
  private readonly jwt_expires_in!: string;

  @Inject("jwt_issuer")
  private readonly jwt_issuer!: string;

  @Inject("jwt_private_key")
  private readonly jwt_private_key!: string;

  @Query(() => SignInObject, { description: "Realiza o sign-in no sistema." })
  public async sign_in(@Args() sign_in: SignInArgs): Promise<SignInObject> {
    const validator = new Validator();

    const user = await User.findOne({
      where: validator.isEmail(sign_in.identifier)
        ? { email: { [Op.eq]: sign_in.identifier } }
        : { username: { [Op.eq]: sign_in.identifier } }
    });

    if (validator.isDefined(user)) {
      if (
        await verify((user as User).password, sign_in.password, this.argon2)
      ) {
        return {
          jwt: await signAsync(
            (user as User).get(),
            readFileSync(resolve(this.jwt_private_key)),
            {
              algorithm: this.jwt_algorithm,
              expiresIn: this.jwt_expires_in,
              issuer: this.jwt_issuer
            }
          )
        };
      } else {
        return incorrect_sign_in$
          .default()()
          .toPromise();
      }
    } else {
      return account_missing$
        .default()()
        .toPromise();
    }
  }
}

// EXPORT - Factory
const factory: TGraphQL<IOption> = option => scheduler => {
  option = assign(option)({
    jwt_algorithm: process.env.JWT_ALGORITHM,
    jwt_expires_in: process.env.JWT_EXPIRES_IN,
    jwt_issuer: process.env.JWT_ISSUER,
    jwt_private_key: process.env.JWT_PRIVATE_KEY
  } as Partial<Omit<IOption, "argon2">>) as IOption;

  const schema = buildSchema({
    container: Container.of(SignInResolver)
      .set("argon2", option.argon2)
      .set("jwt_algorithm", option.jwt_algorithm as string)
      .set("jwt_expires_in", option.jwt_expires_in as string)
      .set("jwt_issuer", option.jwt_issuer as string)
      .set("jwt_private_key", option.jwt_private_key as string),
    resolvers: [SignInResolver]
  });

  return scheduler ? from(schema, scheduler) : from(schema);
};

export default factory;
