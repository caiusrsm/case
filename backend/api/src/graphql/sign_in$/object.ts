// IMPORT - ~/node_modules
import { Field, ObjectType } from "type-graphql";

// EXPORT - Class
@ObjectType({ description: "Sign-in do sistema." })
export class SignInObject {
  @Field({ description: "Token do sign-in." })
  public jwt!: string;
}
