// IMPORT - ~/node_modules
import { IsNotEmpty } from "class-validator";
import { ArgsType, Field } from "type-graphql";

// EXPORT - Class
@ArgsType()
export class SignInArgs {
  @Field({ description: "Nome de usuário ou e-mail." })
  @IsNotEmpty()
  public readonly identifier!: string;

  @Field({ description: "Senha." })
  @IsNotEmpty()
  public readonly password!: string;
}
