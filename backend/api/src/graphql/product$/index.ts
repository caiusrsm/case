// IMPORT
import { readFileSync } from "fs";
import { resolve } from "path";

// IMPORT - ~/node_modules
import { verifyAsync } from "jsonwebtoken-promisified";
import { assign } from "lodash/fp";
import { from } from "rxjs";
import { WhereAttributeHash } from "sequelize/types";
import {
  Arg,
  Args,
  Ctx,
  Int,
  Mutation,
  Query,
  Resolver,
  buildSchema
} from "type-graphql";
import { Container, Inject, Service } from "typedi";

// IMPORT - @/graphql
import { IContext, TGraphQL } from "@/graphql";
import { ProductArgs } from "@/graphql/product$/args";
import { NullableProductInput, ProductInput } from "@/graphql/product$/input";
import { ProductObject } from "@/graphql/product$/object";

// IMPORT - @/helper
import account_missing$ from "@/helper/error/account_missing$";
import forbidden$ from "@/helper/error/http/forbidden$";

// IMPORT - @/model
import Model, { FIELD_CATEGORY_VALUES } from "@/model/Product";
import User from "@/model/User";

// EXPORT - Interface
export interface IOption {
  jwt_algorithm?: string;
  jwt_public_key?: string;
}

// EXPORT - Class
export abstract class Product<T> {
  public readonly _id!: string;
  public readonly bar_code!: string;
  public readonly category!: number;
  public readonly in_stock!: number;
  public readonly name!: string;
  public readonly product_picture!: string;
  public readonly user!: T;
}

@Service()
@Resolver(ProductObject)
export class ProductResolver {
  @Inject("jwt_algorithm")
  private readonly jwt_algorithm!: string;

  @Inject("jwt_public_key")
  private readonly jwt_public_key!: string;

  @Query(() => ProductObject, {
    description: "Retorna um produto.",
    nullable: true
  })
  public async product(
    @Args() { _id }: ProductArgs,
    @Ctx() ctx: IContext
  ): Promise<Model | null> {
    if (ctx.req.headers.authorization) {
      const user = await User.findByPk(
        (((await verifyAsync(
          ctx.req.headers.authorization,
          readFileSync(resolve(this.jwt_public_key)),
          {
            algorithms: [this.jwt_algorithm]
          }
        )) as unknown) as Model)._id
      );

      if (user) {
        return Model.findByPk(_id).then(result => {
          if (result && result.category === FIELD_CATEGORY_VALUES["PRIMARY"]) {
            return forbidden$()().toPromise();
          } else {
            return result;
          }
        });
      } else {
        return account_missing$()().toPromise();
      }
    } else {
      return forbidden$()().toPromise();
    }
  }

  @Query(() => [ProductObject], {
    description: "Retorna uma lista de produtos.",
    nullable: true
  })
  public async products(
    @Ctx() ctx: IContext,
    @Arg("where", { nullable: true }) where?: NullableProductInput
  ): Promise<Model[] | null> {
    if (ctx.req.headers.authorization) {
      const input = where
        ? where
        : { category: FIELD_CATEGORY_VALUES["PRIMARY"] };

      if (input.category === FIELD_CATEGORY_VALUES["SECONDARY"]) {
        return forbidden$()().toPromise();
      } else {
        const user = await User.findByPk(
          (((await verifyAsync(
            ctx.req.headers.authorization,
            readFileSync(resolve(this.jwt_public_key)),
            {
              algorithms: [this.jwt_algorithm]
            }
          )) as unknown) as Model)._id
        );

        if (user) {
          return Model.findAll({
            where: (input as unknown) as WhereAttributeHash
            // eslint-disable-next-line @typescript-eslint/no-extra-parens
          });
        } else {
          return account_missing$()().toPromise();
        }
      }
    } else {
      return forbidden$()().toPromise();
    }
  }

  @Mutation(() => ProductObject, { description: "Cria um produto." })
  public async product_create(
    @Arg("value") value: ProductInput,
    @Ctx() ctx: IContext
  ): Promise<Model> {
    if (ctx.req.headers.authorization) {
      const user = await User.findByPk(
        (((await verifyAsync(
          ctx.req.headers.authorization,
          readFileSync(resolve(this.jwt_public_key)),
          {
            algorithms: [this.jwt_algorithm]
          }
        )) as unknown) as Model)._id
      );

      if (user) {
        const input: ProductInput & { user_id: string } = {
          user_id: user._id,
          ...value
        };

        return Model.create(input);
      } else {
        return account_missing$()().toPromise();
      }
    } else {
      return forbidden$()().toPromise();
    }
  }

  @Mutation(() => Int, { description: "Remove um produto." })
  public async product_remove(
    @Args() { _id }: ProductArgs,
    @Ctx() ctx: IContext
  ): Promise<number> {
    if (ctx.req.headers.authorization) {
      const user = await User.findByPk(
        (((await verifyAsync(
          ctx.req.headers.authorization,
          readFileSync(resolve(this.jwt_public_key)),
          {
            algorithms: [this.jwt_algorithm]
          }
        )) as unknown) as Model)._id
      );

      if (user) {
        return Model.destroy({
          where: { _id, category: FIELD_CATEGORY_VALUES["PRIMARY"] }
        });
      } else {
        return account_missing$()().toPromise();
      }
    } else {
      return forbidden$()().toPromise();
    }
  }

  @Mutation(() => [Int], { description: "Atualiza um produto." })
  public async product_update(
    @Args() { _id }: ProductArgs,
    @Arg("value") value: NullableProductInput,
    @Ctx() ctx: IContext
  ): Promise<[number]> {
    if (ctx.req.headers.authorization) {
      const user = await User.findByPk(
        (((await verifyAsync(
          ctx.req.headers.authorization,
          readFileSync(resolve(this.jwt_public_key)),
          {
            algorithms: [this.jwt_algorithm]
          }
        )) as unknown) as Model)._id
      );

      if (user) {
        return Model.update(value, {
          returning: false,
          where: { _id, category: FIELD_CATEGORY_VALUES["PRIMARY"] }
        }) as Promise<[number]>;
      } else {
        return account_missing$()().toPromise();
      }
    } else {
      return forbidden$()().toPromise();
    }
  }
}

// EXPORT - Factory
const factory: TGraphQL<IOption, "defaulted"> = (option = {}) => scheduler => {
  option = assign(option)({
    jwt_algorithm: process.env.JWT_ALGORITHM,
    jwt_public_key: process.env.JWT_PUBLIC_KEY
  } as Partial<IOption>) as IOption;

  const schema = buildSchema({
    container: Container.of(ProductResolver)
      .set("jwt_algorithm", option.jwt_algorithm)
      .set("jwt_public_key", option.jwt_public_key),
    resolvers: [ProductResolver]
  });

  return scheduler ? from(schema, scheduler) : from(schema);
};

export default factory;
