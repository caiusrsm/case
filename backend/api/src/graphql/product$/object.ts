// IMPORT - ~/node_modules
import { Field, ObjectType } from "type-graphql";

// IMPORT - @/graphql
import { Product } from "@/graphql/product$";
import { UserObject } from "@/graphql/user$/object";

@ObjectType({ description: "Produto do sistema." })
export class ProductObject implements Omit<Product<UserObject>, "password"> {
  @Field({ description: "Código de identificação (UUIDv4)." })
  public readonly _id!: string;

  @Field({ description: "Código de barras." })
  public bar_code!: string;

  @Field({ description: "Categoria." })
  public category!: number;

  @Field({ description: "Quantidade." })
  public in_stock!: number;

  @Field({ description: "Nome." })
  public name!: string;

  @Field({ description: "Foto." })
  public product_picture!: string;

  @Field(() => UserObject, { description: "Usuário." })
  public user!: UserObject;
}
