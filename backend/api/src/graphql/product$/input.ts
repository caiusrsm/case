// IMPORT - ~/node_modules
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsUrl,
  MaxLength,
  MinLength
} from "class-validator";
import { Field, InputType } from "type-graphql";

// IMPORT - @/graphql
import { Product } from "@/graphql/product$";

// IMPORT - @/model
import {
  FIELD_BAR_CODE_MAX_LENGTH,
  FIELD_BAR_CODE_MIN_LENGTH,
  FIELD_CATEGORY_VALUES,
  FIELD_NAME_MAX_LENGTH
} from "@/model/Product";

// EXPORT - Class
@InputType({ description: "Produto do sistema." })
export class NullableProductInput
  implements Omit<Product<any>, "_id" | "user"> {
  @Field({ description: "Código de barras.", nullable: true })
  @IsNotEmpty()
  @IsNumberString()
  @MaxLength(FIELD_BAR_CODE_MAX_LENGTH)
  @MinLength(FIELD_BAR_CODE_MIN_LENGTH)
  public bar_code!: string;

  @Field({ description: "Categoria.", nullable: true })
  @IsEnum(FIELD_CATEGORY_VALUES)
  @IsNotEmpty()
  public category!: number;

  @Field({ description: "Quantidade.", nullable: true })
  @IsInt()
  @IsNotEmpty()
  public in_stock!: number;

  @Field({ description: "Nome.", nullable: true })
  @IsNotEmpty()
  @MaxLength(FIELD_NAME_MAX_LENGTH)
  public name!: string;

  @Field({ description: "Foto.", nullable: true })
  @IsNotEmpty()
  @IsUrl()
  public product_picture!: string;
}

@InputType({ description: "Usuário do sistema." })
export class ProductInput implements Omit<Product<any>, "_id" | "user"> {
  @Field({ description: "Código de barras." })
  @IsNotEmpty()
  @IsNumberString()
  @MaxLength(FIELD_BAR_CODE_MAX_LENGTH)
  @MinLength(FIELD_BAR_CODE_MIN_LENGTH)
  public bar_code!: string;

  @Field({ description: "Categoria." })
  @IsEnum(FIELD_CATEGORY_VALUES)
  @IsNotEmpty()
  public category!: number;

  @Field({ description: "Quantidade." })
  @IsInt()
  @IsNotEmpty()
  public in_stock!: number;

  @Field({ description: "Nome." })
  @IsNotEmpty()
  @MaxLength(FIELD_NAME_MAX_LENGTH)
  public name!: string;

  @Field({ description: "Foto." })
  @IsNotEmpty()
  @IsUrl()
  public product_picture!: string;
}
