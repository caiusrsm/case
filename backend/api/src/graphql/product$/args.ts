// IMPORT - ~/node_modules
import { IsNotEmpty, IsUUID } from "class-validator";
import { ArgsType, Field } from "type-graphql";

// IMPORT - @/graphql
import { Product } from "@/graphql/product$";

// EXPORT - Class
@ArgsType()
export class ProductArgs implements Pick<Product<any>, "_id"> {
  @Field({ description: "Código de identificação (UUIDv4)." })
  @IsNotEmpty()
  @IsUUID("4")
  public readonly _id!: string;
}
