// IMPORT - ~/node_modules
import { HttpRequest } from "@marblejs/core";
import { GraphQLSchema } from "graphql";
import { Observable, SchedulerLike } from "rxjs";

// EXPORT - Interface
export interface IContext {
  req: HttpRequest;
}

// EXPORT - Type
export type TGraphQL<
  T,
  U extends "defaulted" | null = null
> = U extends "defaulted"
  ? (option?: T) => (scheduler?: SchedulerLike) => Observable<GraphQLSchema>
  : (option: T) => (scheduler?: SchedulerLike) => Observable<GraphQLSchema>;

// EXPORT - Sub-Module
import * as product$ from "@/graphql/product$";
import * as sign_in$ from "@/graphql/sign_in$";
import * as user$ from "@/graphql/user$";

export { product$, sign_in$, user$ };
