// IMPORT
import { readFileSync } from "fs";
import { resolve } from "path";

// IMPORT - ~/node_modules
import { Options, hash } from "argon2";
import { Validator } from "class-validator";
import { verifyAsync } from "jsonwebtoken-promisified";
import { assign } from "lodash/fp";
import { from } from "rxjs";
import { WhereAttributeHash } from "sequelize/types";
import {
  Arg,
  Args,
  Ctx,
  Int,
  Mutation,
  Query,
  Resolver,
  buildSchema
} from "type-graphql";
import { Container, Inject, Service } from "typedi";

// IMPORT - @/graphql
import { IContext, TGraphQL } from "@/graphql";
import { UserArgs } from "@/graphql/user$/args";
import { NullableUserInput, UserInput } from "@/graphql/user$/input";
import { UserObject } from "@/graphql/user$/object";

// IMPORT - @/helper
import account_missing$ from "@/helper/error/account_missing$";
import forbidden$ from "@/helper/error/http/forbidden$";

// IMPORT - @/model
import Model from "@/model/User";

// EXPORT - Constant
export const FIELD_PASSWORD_MIN_LENGTH = 8;

// EXPORT - Interface
export interface IOption {
  argon2: Options & { raw?: false };
  jwt_algorithm?: string;
  jwt_public_key?: string;
}

// EXPORT - Class
export abstract class User<T> {
  public readonly _id!: string;
  public readonly email!: string;
  public readonly name!: string;
  public readonly password!: string;
  public readonly products!: T;
  public readonly username!: string;
}

@Service()
@Resolver(UserObject)
export class UserResolver {
  @Inject("argon2")
  private readonly argon2!: Options & { raw?: false };

  @Inject("jwt_algorithm")
  private readonly jwt_algorithm!: string;

  @Inject("jwt_public_key")
  private readonly jwt_public_key!: string;

  @Query(() => UserObject, {
    description: "Retorna um usuário.",
    nullable: true
  })
  public async user(@Args() { _id }: UserArgs): Promise<Model | null> {
    return Model.findByPk(_id);
  }

  @Query(() => [UserObject], {
    description: "Retorna uma lista de usuários.",
    nullable: true
  })
  public async users(
    @Arg("where", { nullable: true }) where?: NullableUserInput
  ): Promise<Model[] | null> {
    return Model.findAll({
      where: (where as unknown) as WhereAttributeHash
      // eslint-disable-next-line @typescript-eslint/no-extra-parens
    }).then(result => (new Validator().arrayNotEmpty(result) ? result : null));
  }

  @Mutation(() => UserObject, { description: "Cria um usuário." })
  public async user_create(@Arg("value") value: UserInput): Promise<Model> {
    const input = { ...value };

    input.password = await hash(value.password, this.argon2);

    return Model.create(input);
  }

  @Mutation(() => Int, { description: "Remove um usuário." })
  public async user_remove(@Ctx() ctx: IContext): Promise<number> {
    if (ctx.req.headers.authorization) {
      const user = await Model.findByPk(
        (((await verifyAsync(
          ctx.req.headers.authorization,
          readFileSync(resolve(this.jwt_public_key)),
          {
            algorithms: [this.jwt_algorithm]
          }
        )) as unknown) as Model)._id
      );

      if (user) {
        return Model.destroy({ where: { _id: user._id } });
      } else {
        return account_missing$()().toPromise();
      }
    } else {
      return forbidden$()().toPromise();
    }
  }

  @Mutation(() => [Int], { description: "Atualiza um usuário." })
  public async user_update(
    @Arg("value") value: NullableUserInput,
    @Ctx() ctx: IContext
  ): Promise<[number]> {
    if (ctx.req.headers.authorization) {
      const user = await Model.findByPk(
        (((await verifyAsync(
          ctx.req.headers.authorization,
          readFileSync(resolve(this.jwt_public_key)),
          {
            algorithms: [this.jwt_algorithm]
          }
        )) as unknown) as Model)._id
      );

      if (user) {
        if (value.password) {
          const input = { ...value };

          input.password = await hash(user.password, this.argon2);

          return Model.update(input, {
            returning: false,
            where: { _id: user._id }
          }) as Promise<[number]>;
        } else {
          return Model.update(value, {
            returning: false,
            where: { _id: user._id }
          }) as Promise<[number]>;
        }
      } else {
        return account_missing$()().toPromise();
      }
    } else {
      return forbidden$()().toPromise();
    }
  }
}

// EXPORT - Factory
const factory: TGraphQL<IOption> = option => scheduler => {
  option = assign(option)({
    jwt_algorithm: process.env.JWT_ALGORITHM,
    jwt_private_key: process.env.JWT_PRIVATE_KEY,
    jwt_public_key: process.env.JWT_PUBLIC_KEY
  } as Partial<Omit<IOption, "argon2">>) as IOption;

  const schema = buildSchema({
    container: Container.of(UserResolver)
      .set("argon2", option.argon2)
      .set("jwt_algorithm", option.jwt_algorithm)
      .set("jwt_public_key", option.jwt_public_key),
    resolvers: [UserResolver]
  });

  return scheduler ? from(schema, scheduler) : from(schema);
};

export default factory;
