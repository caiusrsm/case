// IMPORT - ~/node_modules
import {
  IsEmail,
  IsNotEmpty,
  Matches,
  MaxLength,
  MinLength
} from "class-validator";
import { Field, InputType } from "type-graphql";

// IMPORT - @/graphql
import { FIELD_PASSWORD_MIN_LENGTH, User } from "@/graphql/user$";

// IMPORT - @/model
import { FIELD_NAME_MAX_LENGTH, FIELD_USERNAME_MIN_LENGTH } from "@/model/User";

// EXPORT - Class
@InputType({ description: "Usuário do sistema." })
export class NullableUserInput implements Omit<User<any>, "_id" | "products"> {
  @Field({ description: "E-mail.", nullable: true })
  @IsEmail()
  @IsNotEmpty()
  public readonly email!: string;

  @Field({ description: "Nome completo.", nullable: true })
  @IsNotEmpty()
  @MaxLength(FIELD_NAME_MAX_LENGTH)
  public readonly name!: string;

  @Field({ description: "Senha.", nullable: true })
  @IsNotEmpty()
  @Matches(/[a-z]+/)
  @Matches(/[A-Z]+/)
  @Matches(/[\d]+/)
  @Matches(/[^a-zA-Z0-9\s]+/)
  @MinLength(FIELD_PASSWORD_MIN_LENGTH)
  public readonly password!: string;

  @Field({ description: "Nome de usuário.", nullable: true })
  @IsNotEmpty()
  @Matches(/^[A-ú0-9]+(?:[-_().&%$#@!]+|[A-ú0-9]+)*$/)
  @MinLength(FIELD_USERNAME_MIN_LENGTH)
  public readonly username!: string;
}

@InputType({ description: "Usuário do sistema." })
export class UserInput implements Omit<User<any>, "_id" | "products"> {
  @Field({ description: "E-mail." })
  @IsEmail()
  @IsNotEmpty()
  public readonly email!: string;

  @Field({ description: "Nome completo." })
  @IsNotEmpty()
  @MaxLength(FIELD_NAME_MAX_LENGTH)
  public readonly name!: string;

  @Field({ description: "Senha." })
  @IsNotEmpty()
  @Matches(/[a-z]+/)
  @Matches(/[A-Z]+/)
  @Matches(/[\d]+/)
  @Matches(/[^a-zA-Z0-9\s]+/)
  @MinLength(FIELD_PASSWORD_MIN_LENGTH)
  public readonly password!: string;

  @Field({ description: "Nome de usuário." })
  @IsNotEmpty()
  @Matches(/^[A-ú0-9]+(?:[-_().&%$#@!]+|[A-ú0-9]+)*$/)
  @MinLength(FIELD_USERNAME_MIN_LENGTH)
  public readonly username!: string;
}
