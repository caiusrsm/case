// IMPORT - ~/node_modules
import { Field, ObjectType } from "type-graphql";

// IMPORT - @/graphql
import { ProductObject } from "@/graphql/product$/object";
import { User } from "@/graphql/user$";

@ObjectType({ description: "Usuário do sistema." })
export class UserObject implements Omit<User<ProductObject>, "password"> {
  @Field({ description: "Código de identificação (UUIDv4)." })
  public readonly _id!: string;

  @Field({ description: "E-mail." })
  public readonly email!: string;

  @Field({ description: "Nome completo." })
  public readonly name!: string;

  @Field(() => [ProductObject], { description: "Produtos." })
  public readonly products!: ProductObject;

  @Field({ description: "Nome de usuário." })
  public readonly username!: string;
}
