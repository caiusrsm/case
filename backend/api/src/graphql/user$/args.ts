// IMPORT - ~/node_modules
import { IsNotEmpty, IsUUID } from "class-validator";
import { ArgsType, Field } from "type-graphql";

// IMPORT - @/graphql
import { User } from "@/graphql/user$";

// EXPORT - Class
@ArgsType()
export class UserArgs implements Pick<User<any>, "_id"> {
  @Field({ description: "Código de identificação (UUIDv4)." })
  @IsNotEmpty()
  @IsUUID("4")
  public readonly _id!: string;
}
