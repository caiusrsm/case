// IMPORT - ~/node_modules
import { Options } from "argon2";
import { assign } from "lodash/fp";
import { of } from "rxjs";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// EXPORT - Factory
const factory: TSetting<Partial<Options>, Options, "defaulted"> = (
  option = {}
) => scheduler => {
  const argon2_option = assign(option)({
    hashLength: (process.env.ARGON2_HASH_LENGTH as unknown) as number,
    memoryCost: (process.env.ARGON2_MEMORY_COST as unknown) as number,
    parallelism: (process.env.ARGON2_PARALLELISM as unknown) as number,
    timeCost: (process.env.ARGON2_TIME_COST as unknown) as number,
    type: (process.env.ARGON2_TYPE as unknown) as Options["type"]
  } as Options);

  return scheduler ? of(argon2_option, scheduler) : of(argon2_option);
};

export default factory;
