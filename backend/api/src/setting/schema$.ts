// IMPORT - ~/node_modules
import { GraphQLSchema } from "graphql";
import { mergeSchemas } from "graphql-tools";
import { of } from "rxjs";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// EXPORT - Factory
const factory: TSetting<
  GraphQLSchema[],
  GraphQLSchema
> = schemas => scheduler => {
  const schema = mergeSchemas({
    schemas
  });

  return scheduler ? of(schema, scheduler) : of(schema);
};

export default factory;
