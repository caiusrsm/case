// IMPORT - ~/node_modules
import { from, of } from "rxjs";
import { mapTo, mergeMap } from "rxjs/operators";
import { Sequelize } from "sequelize-typescript";
import { Logger } from "winston";

// IMPORT - @/model
import Product from "@/model/Product";
import User from "@/model/User";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// EXPORT - Factory
const factory: TSetting<Logger, Sequelize> = logger => scheduler => {
  const sequelize = new Sequelize(process.env.DATABASE_URI as string, {
    dialectOptions: {
      timezone: process.env.DATABASE_TIMEZONE as string
    },
    logging: (message: string) =>
      logger.log({
        level: process.env.LOGGER_LEVEL_CONSOLE as string,
        message
      }),
    models: [Product, User],
    pool: {
      acquire: Number(process.env.DATABASE_POOL_ACQUIRE as string),
      idle: Number(process.env.DATABASE_POOL_IDLE as string),
      max: Number(process.env.DATABASE_POOL_MAX as string),
      min: Number(process.env.DATABASE_POOL_MIN as string)
    },
    timezone: process.env.DATABASE_TIMEZONE as string
  });

  return (scheduler ? of(sequelize, scheduler) : of(sequelize)).pipe(
    mergeMap(instance =>
      from(
        instance
          .authenticate()
          .then(() =>
            logger.log({
              level: process.env.LOGGER_LEVEL_CONSOLE as string,
              message: "Connection has been established successfully."
            })
          )
          .catch(error =>
            logger.log({
              level: process.env.LOGGER_LEVEL_ERROR as string,
              message: "Unable to connect to the database: ",
              meta: { error }
            })
          )
      ).pipe(mapTo(instance))
    )
  );
};

export default factory;
