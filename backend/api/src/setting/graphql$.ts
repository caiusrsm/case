// IMPORT - ~/node_modules
import { GraphQLSchema } from "graphql";
import { forkJoin } from "rxjs";

// IMPORT - @/graphql
import product$, { IOption as product_option } from "@/graphql/product$";
import sign_in$, { IOption as sign_in_option } from "@/graphql/sign_in$";
import user$, { IOption as user_option } from "@/graphql/user$";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// EXPORT - Interface
export interface IGraphQL {
  product?: product_option;
  sign_in: sign_in_option;
  user: user_option;
}

// EXPORT - Factory
const factory: TSetting<IGraphQL, GraphQLSchema[]> = option => scheduler =>
  forkJoin([
    product$(option.product)(scheduler),
    sign_in$(option.sign_in)(scheduler),
    user$(option.user)(scheduler)
  ]);

export default factory;
