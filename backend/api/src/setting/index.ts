// IMPORT - ~/node_modules
import { Observable, SchedulerLike } from "rxjs";

// EXPORT - Type
export type TSetting<
  T,
  U = T,
  V extends "defaulted" | null = null
> = V extends "defaulted"
  ? (option?: T) => (scheduler?: SchedulerLike) => Observable<U>
  : (option: T) => (scheduler?: SchedulerLike) => Observable<U>;

// EXPORT - Sub-Module
import * as argon2$ from "@/setting/argon2$";
import * as graphql$ from "@/setting/graphql$";
import * as model$ from "@/setting/model$";
import * as playground$ from "@/setting/playground$";
import * as schema$ from "@/setting/schema$";
import * as sequelize$ from "@/setting/sequelize$";
import * as winston$ from "@/setting/winston$";

export {
  argon2$,
  graphql$,
  model$,
  playground$,
  schema$,
  sequelize$,
  winston$
};
