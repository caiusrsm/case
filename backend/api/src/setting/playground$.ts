// IMPORT - ~/node_modules
import {
  CursorShape,
  RenderPageOptions,
  Theme
} from "graphql-playground-html/dist/render-playground-page";
import { assign } from "lodash/fp";
import { of } from "rxjs";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// EXPORT - Factory
const factory: TSetting<
  Partial<RenderPageOptions>,
  RenderPageOptions,
  "defaulted"
> = (option = {}) => scheduler => {
  const playground_option = assign(option)({
    endpoint: process.env.PLAYGROUND_ENDPOINT as string,
    settings: {
      "editor.cursorShape": process.env
        .PLAYGROUND_SETTINGS_EDITOR_CURSOR as CursorShape,
      "editor.fontFamily": process.env
        .PLAYGROUND_SETTINGS_EDITOR_FONT_FAMILY as string,
      "editor.fontSize": (process.env
        .PLAYGROUND_SETTINGS_EDITOR_FONT_SIZE as unknown) as number,
      "editor.reuseHeaders": (process.env
        .PLAYGROUND_SETTINGS_EDITOR_REUSE_HEADERS as unknown) as boolean,
      "editor.theme": process.env.PLAYGROUND_SETTINGS_EDITOR_THEME as Theme,
      "general.betaUpdates": (process.env
        .PLAYGROUND_SETTINGS_GENERAL_BETA_UPDATES as unknown) as boolean,
      "request.credentials": process.env
        .PLAYGROUND_SETTINGS_REQUEST_CREDENTIALS as string,
      "tracing.hideTracingResponse": (process.env
        .PLAYGROUND_SETTINGS_TRACING_HIDE_TRACKING_RESPONSE as unknown) as boolean
    },
    subscriptionEndpoint: process.env.PLAYGROUND_ENDPOINT_SUBSCRIPTION
  } as RenderPageOptions);

  return scheduler ? of(playground_option, scheduler) : of(playground_option);
};

export default factory;
