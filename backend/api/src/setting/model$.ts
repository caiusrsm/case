// IMPORT - ~/node_modules
import { forkJoin, from } from "rxjs";

// IMPORT - @/model
import Product from "@/model/Product";
import User from "@/model/User";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// EXPORT - Factory
const factory: TSetting<void, unknown[], "defaulted"> = () => scheduler => {
  const product = scheduler
    ? from(Product.sync(), scheduler)
    : from(Product.sync());
  const user = scheduler ? from(User.sync(), scheduler) : from(User.sync());

  return forkJoin([user, product]);
};

export default factory;
