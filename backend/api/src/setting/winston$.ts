// IMPORT - ~/node_modules
import path from "path";
import { assign } from "lodash/fp";
import { of } from "rxjs";
import { Logger, addColors, createLogger, format, transports } from "winston";
// eslint-disable-next-line import/default
import DailyRotateFile from "winston-daily-rotate-file";
import {
  AbstractConfigSetColors,
  AbstractConfigSetLevels
} from "winston/lib/winston/config";

// IMPORT - @/setting
import { TSetting } from "@/setting";

// DESTRUCTURE
const { colorize, combine, printf, splat } = format;
const { Console } = transports;

// EXPORT - Factory
const factory: TSetting<
  {
    colour: Partial<AbstractConfigSetColors>;
    level: Partial<AbstractConfigSetLevels>;
  },
  Logger,
  "defaulted"
> = (option = { colour: {}, level: {} }) => scheduler => {
  addColors(
    assign(option.colour)({
      alert: process.env.LOGGER_FORMAT_ALERT as string,
      crit: process.env.LOGGER_FORMAT_CRIT as string,
      debug: process.env.LOGGER_FORMAT_DEBUG as string,
      emerg: process.env.LOGGER_FORMAT_EMERG as string,
      error: process.env.LOGGER_FORMAT_ERROR as string,
      info: process.env.LOGGER_FORMAT_INFO as string,
      notice: process.env.LOGGER_FORMAT_NOTICE as string,
      warning: process.env.LOGGER_FORMAT_WARNING as string
    } as AbstractConfigSetColors)
  );

  const log_format = combine(
    colorize(),
    splat(),
    printf(info =>
      info.meta && info.meta.error
        ? `${info.level} -> ${info.message} ${info.meta.error.stack}`
        : `${info.level} -> ${info.message}`
    )
  );

  const logger = createLogger({
    exitOnError: false,
    levels: assign(option.level)({
      alert: (process.env.LOGGER_INDEX_ALERT as unknown) as number,
      crit: (process.env.LOGGER_INDEX_CRIT as unknown) as number,
      debug: (process.env.LOGGER_INDEX_DEBUG as unknown) as number,
      emerg: (process.env.LOGGER_INDEX_EMERG as unknown) as number,
      error: (process.env.LOGGER_INDEX_ERROR as unknown) as number,
      info: (process.env.LOGGER_INDEX_INFO as unknown) as number,
      notice: (process.env.LOGGER_INDEX_NOTICE as unknown) as number,
      warning: (process.env.LOGGER_INDEX_WARNING as unknown) as number
    } as AbstractConfigSetLevels),
    transports: [
      new Console({
        format: log_format,
        handleExceptions: true,
        level: process.env.LOGGER_LEVEL_CONSOLE
      }),
      new DailyRotateFile({
        filename: path.resolve(
          process.env.LOGGER_PATH as string,
          `access-${process.env.NODE_ENV}-%DATE%.log`
        ),
        format: log_format,
        handleExceptions: true,
        level: process.env.LOGGER_LEVEL_CONSOLE
      }),
      new DailyRotateFile({
        filename: path.resolve(
          process.env.LOGGER_PATH as string,
          `error-${process.env.NODE_ENV}-%DATE%.log`
        ),
        format: log_format,
        handleExceptions: true,
        level: process.env.LOGGER_LEVEL_ERROR
      })
    ]
  });

  return scheduler ? of(logger, scheduler) : of(logger);
};

export default factory;
