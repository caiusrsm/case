// IMPORT - ~/node_modules
import { EffectMetadata, HttpRequest, HttpResponse } from "@marblejs/core";
import { assign } from "lodash/fp";
import { fromEvent } from "rxjs";
import { filter, mapTo, take, tap } from "rxjs/operators";
import { Logger } from "winston";

// IMPORT - @/middleware
import { TMiddleware } from "@/middleware";

// EXPORT - Interface
export interface ILogger {
  level?: string;
  logger: Logger;
  message?: string | TMessage;
  silent?: boolean;
}

// EXPORT - Type
export type TMessage = (
  req: HttpRequest,
  res: HttpResponse,
  meta?: EffectMetadata
) => string;

// EXPORT - Constant
export const message: { [key: string]: TMessage } = {
  COMBINED: (req, res) =>
    `[${new Date()}]: ${req.headers["x-forwarded-for"] ??
      req.connection.remoteAddress ??
      req.socket.remoteAddress} "${req.method} ${req.url} HTTP/${
      req.httpVersion
    }" ${res.statusMessage}(${res.statusCode}) ${res.getHeader(
      "content-length"
    )} ${req.headers.referer} ${req.headers["user-agent"]}`,
  COMMON: (req, res) =>
    `[${new Date()}]: ${req.headers["x-forwarded-for"] ??
      req.connection.remoteAddress ??
      req.socket.remoteAddress} "${req.method} ${req.url} HTTP/${
      req.httpVersion
    }" ${res.statusMessage}(${res.statusCode}) ${res.getHeader(
      "content-length"
    )}`,
  DEV: (req, res) =>
    `[${new Date()}]: ${req.method} ${req.url} ${
      res.statusCode
    } ${res.getHeader("content-length")}`,
  SHORT: (req, res) =>
    `[${new Date()}]: ${req.headers["x-forwarded-for"] ??
      req.connection.remoteAddress ??
      req.socket.remoteAddress} ${req.method} ${req.url} HTTP/${
      req.httpVersion
    } ${res.statusCode} ${res.getHeader("content-length")}`,
  TINY: (req, res) =>
    `[${new Date()}]: ${req.method} ${req.url} ${res.statusCode}`
};

// EXPORT - Factory
const factory: TMiddleware<ILogger> = option => (req$, res, meta) => {
  const logger_option = assign<ILogger>(option)({
    level: process.env.LOGGER_LEVEL_ACCESS as string,
    message: process.env.LOGGER_MESSAGE as string,
    silent: false
  });

  return req$.pipe(
    tap(req =>
      fromEvent(res, "finish")
        .pipe(
          // eslint-disable-next-line @typescript-eslint/no-magic-numbers
          take(1),
          filter(() => !logger_option.silent),
          mapTo(
            typeof logger_option.message === "string"
              ? message[logger_option.message](req, res, meta)
              : logger_option.message(req, res, meta)
          )
        )
        .subscribe(entry =>
          logger_option.logger.log({
            level: logger_option.level,
            message: entry,
            meta
          })
        )
    )
  );
};

export default factory;
