// IMPORT - ~/node_modules
import { HttpMiddlewareEffect } from "@marblejs/core";

// EXPORT - Type
export type TMiddleware<
  T,
  U extends "defaulted" | null = null
> = U extends "defaulted"
  ? (option?: T) => HttpMiddlewareEffect
  : (option: T) => HttpMiddlewareEffect;

// EXPORT - Sub-Module
import * as logger$ from "@/middleware/logger$";

export { logger$ };
