// EXPORT - Sub-Module
import * as Product from "@/model/Product";
import * as User from "@/model/User";

export { Product, User };
