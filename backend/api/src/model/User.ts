// IMPORT - ~/node_modules
import { Expose } from "class-transformer";
import {
  IsEmail,
  IsNotEmpty,
  IsUUID,
  Matches,
  MaxLength,
  MinLength
} from "class-validator";
import {
  Column,
  DataType,
  Default,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";

// IMPORT - @/model
import Product from "@/model/Product";

// EXPORT - Constant
export const FIELD_NAME_MAX_LENGTH = 200;
export const FIELD_USERNAME_MIN_LENGTH = 4;

// EXPORT - Class
@Table({ comment: "Usuário do sistema." })
export default class User extends Model {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Column({
    comment: "Código de identificação (UUIDv4).",
    type: DataType.UUID
  })
  @Expose()
  @IsNotEmpty()
  @IsUUID("4")
  public readonly _id!: string;

  @Unique
  @Column({ comment: "E-mail." })
  @Expose()
  @IsEmail()
  @IsNotEmpty()
  public email!: string;

  @Column({ comment: "Nome completo." })
  @Expose()
  @IsNotEmpty()
  @MaxLength(FIELD_NAME_MAX_LENGTH)
  public name!: string;

  @Column({ comment: "Senha hasheada (Argon2I)." })
  @Expose()
  @IsNotEmpty()
  @Matches(
    /^\$argon2(id|d|i)\$v=(\d+)\$m=(\d+),t=(\d+),p=(\d+)\$([A-Za-z0-9+/=]+)\$([A-Za-z0-9+/=]*)$/
  )
  public password!: string;

  @Unique
  @Column({ comment: "Nome de usuário." })
  @Expose()
  @IsNotEmpty()
  @Matches(/^[A-ú0-9]+(?:[-_().&%$#@!]+|[A-ú0-9]+)*$/)
  @MinLength(FIELD_USERNAME_MIN_LENGTH)
  public username!: string;

  @HasMany(() => Product, { onUpdate: "CASCADE" })
  public products!: Product[];
}
