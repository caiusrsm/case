// IMPORT - ~/node_modules
import { Expose } from "class-transformer";
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsUUID,
  IsUrl,
  MaxLength,
  MinLength
} from "class-validator";
import {
  BelongsTo,
  Column,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";

// IMPORT - @/model
import User from "@/model/User";

// EXPORT - Enum
export enum FIELD_CATEGORY_VALUES {
  "PRIMARY" = 1,
  "SECONDARY" = 2
}

// EXPORT - Constant
export const FIELD_BAR_CODE_MAX_LENGTH = 14;
export const FIELD_BAR_CODE_MIN_LENGTH = 14;
export const FIELD_NAME_MAX_LENGTH = 500;

// EXPORT - Class
@Table({ comment: "Produto do sistema." })
export default class Product extends Model {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Column({
    comment: "Código de identificação (UUIDv4).",
    type: DataType.UUID
  })
  @Expose()
  @IsNotEmpty()
  @IsUUID("4")
  public readonly _id!: string;

  @Column({ comment: "Código de barras." })
  @Expose()
  @IsNotEmpty()
  @IsNumberString()
  @MaxLength(FIELD_BAR_CODE_MAX_LENGTH)
  @MinLength(FIELD_BAR_CODE_MIN_LENGTH)
  public bar_code!: string;

  @Column({ comment: "Categoria." })
  @Expose()
  @IsEnum(FIELD_CATEGORY_VALUES)
  @IsNotEmpty()
  public category!: number;

  @Column({ comment: "Quantidade." })
  @Expose()
  @IsInt()
  @IsNotEmpty()
  public in_stock!: number;

  @Column({ comment: "Nome." })
  @Expose()
  @IsNotEmpty()
  @MaxLength(FIELD_NAME_MAX_LENGTH)
  public name!: string;

  @Unique
  @Column({ comment: "Foto." })
  @Expose()
  @IsNotEmpty()
  @IsUrl()
  public product_picture!: string;

  @ForeignKey(() => User)
  @Column({
    comment: "Código de identificação do usuário (UUIDv4).",
    type: DataType.UUID
  })
  @Expose()
  @IsNotEmpty()
  @IsUUID("4")
  public user_id!: string;

  @BelongsTo(() => User)
  public user!: User;
}
