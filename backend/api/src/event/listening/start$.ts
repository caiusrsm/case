// IMPORT - ~/node_modules
import { ServerEvent, matchEvent } from "@marblejs/core";
import { assign } from "lodash/fp";
import { tap } from "rxjs/operators";

// IMPORT - @/event
import { TEvent } from "@/event";

// EXPORT - Interface
export interface IStart {
  env: string;
  system_name: string;
}

// EXPORT - Factory
const factory: TEvent<Partial<IStart>, "defaulted"> = option => event$ => {
  const start_option = assign(option ? option : {})({
    env: process.env.NODE_ENV as string,
    system_name: process.env.SYSTEM_NAME as string
  } as IStart);

  return event$.pipe(
    matchEvent(ServerEvent.listening),
    tap(({ payload: { host, port } }) =>
      console.log(
        `Running ${start_option.system_name}(${start_option.env}) @ http://${host}:${port}`
      )
    )
  );
};

export default factory;
