// IMPORT - ~/node_modules
import { HttpServerEffect } from "@marblejs/core";

// EXPORT - Type
export type TEvent<
  T,
  U extends "defaulted" | null = null
> = U extends "defaulted"
  ? (option?: T) => HttpServerEffect
  : (option: T) => HttpServerEffect;

// EXPORT - Sub-Module
import * as listening from "@/event/listening";

export { listening };
