// IMPORT - @/helper
import error$, { IOption, TError } from "@/helper/error";

// EXPORT - Factory
const factory: TError<string, "defaulted"> = (
  message = "Account not found."
) => scheduler =>
  error$({ message, name: "AccountMissingError" } as IOption)(scheduler);

export default factory;
