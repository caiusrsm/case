// IMPORT - ~/node_modules
import { HttpError, HttpStatus } from "@marblejs/core";
import { Observable, SchedulerLike, throwError } from "rxjs";

// EXPORT - Interface
export interface IHttpOption {
  _interface_: "IHttpOption";
  context?: string;
  data?: object;
  message: string;
  status: HttpStatus;
}

export interface IOption {
  _interface_: "IOption";
  message: string;
  name: string;
}

// EXPORT - Type
export type TError<
  T,
  U extends "defaulted" | null = null
> = U extends "defaulted"
  ? (option?: T) => (scheduler?: SchedulerLike) => Observable<never>
  : (option: T) => (scheduler?: SchedulerLike) => Observable<never>;

// EXPORT - Type Guard
export const isIHttpOption = (
  error: IHttpOption | IOption
): error is IHttpOption => error._interface_ === "IHttpOption";

export const isIOption = (error: IHttpOption | IOption): error is IOption =>
  error._interface_ === "IOption";

// EXPORT - Factory
const factory: TError<IHttpOption | IOption> = option => scheduler => {
  let error: HttpError | { message: string; name: string };

  if (isIHttpOption(option)) {
    error = new HttpError(
      option.message,
      option.status,
      option.data,
      option.context
    );
  } else {
    error = { message: option.message, name: option.name };
  }

  return scheduler ? throwError(error, scheduler) : throwError(error);
};

export default factory;

// EXPORT - Sub-Module
import * as account_missing$ from "@/helper/error/account_missing$";
import * as http from "@/helper/error/http";
import * as incorrect_sign_in$ from "@/helper/error/incorrect_sign_in$";

export { account_missing$, http, incorrect_sign_in$ };
