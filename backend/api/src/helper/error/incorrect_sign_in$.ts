// IMPORT - @/helper
import error$, { IOption, TError } from "@/helper/error";

// EXPORT - Factory
const factory: TError<string, "defaulted"> = (
  message = "Wrong sign-in information."
) => scheduler =>
  error$({ message, name: "IncorrectSignInError" } as IOption)(scheduler);

export default factory;
