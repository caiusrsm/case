// IMPORT - ~/node_modules
import { HttpStatus } from "@marblejs/core";

// IMPORT - @/helper
import error$, { IHttpOption, TError } from "@/helper/error";

// EXPORT - Factory
const factory: TError<Omit<IHttpOption, "status">, "defaulted"> = (
  option = { message: "Route not found." } as IHttpOption
) => scheduler =>
  error$({
    context: option.context,
    data: option.data,
    message: option.message,
    status: HttpStatus.NOT_FOUND
  } as IHttpOption)(scheduler);

export default factory;
