// IMPORT - ~
require("./dotenv-flow.config");

module.exports = {
  plugins: [["transform-inline-environment-variables"]],
  presets: [
    [
      "@vue/cli-plugin-babel/preset",
      { exclude: ["@babel/plugin-transform-classes"] }
    ]
  ]
};
