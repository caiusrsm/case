Node Backend
------
###### Project ID: 16970087

Backend in NodeJS using GraphQL, MarbleJS and RxJS.

https://graphql.org/graphql-js/ https://docs.marblejs.com/ https://rxjs-dev.firebaseapp.com/

## Dependencies
- [Docker](https://docs.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [OpenSSL](https://openssl.org/)

## Project Setup
```shell
docker login -u ${GIT_LOGIN} https://registry.gitlab.com/
```

## Project Start-Up
```shell
DATABASE=case_node-backend_alpha docker-compose up -d
```

## Project Shut-Down
```shell
docker-compose down
```

## Generate Keys'
```shell
openssl genrsa -out backend/api/key/${NODE_ENV}/private-key.pem 1024 && openssl rsa -in backend/api/key/${NODE_ENV}/private-key.pem -out backend/api/key/${NODE_ENV}/public-key.pem -outform PEM -pubout
```

## Obs
```
O sistema funciona utilizando o docker-compose em conjunto com algumas imagens de utilidades, entre elas uma imagem pré-cacheada do generenciador de pacotes nix com dependências do NodeJS.

Os pacotes type-graphql e sequelize-typescript são usados para prover interfaces de anotação para a declaração das entidades de maneira mais simples.

O sistema usa alguns scripts pré-definidos para integração e deploy contínuo baseado em arquivos de configuração .json, lidos pelo jq e "pipeados" para uma série de funcionalidades padrão do UNIX.

Possíveis melhorias para o sistema seriam a implementação de um controle de processos filhos para permitir testes e2e, middlewares de type-graphql para facilitar a validação do token JWT e o reporting de erros do endpoint, implementação de descobrimento automático de IP para melhor compartilhamento de recursos com outras instâncias do docker e traits para herança múltipla.
```
