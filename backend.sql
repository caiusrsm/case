CREATE TABLE IF NOT EXISTS `Users` (
  `_id` CHAR(36) BINARY  COMMENT 'Código de identificação (UUIDv4).',
  `email` VARCHAR(255) UNIQUE COMMENT 'E-mail.',
  `name` VARCHAR(255) COMMENT 'Nome completo.',
  `password` VARCHAR(255) COMMENT 'Senha hasheada (Argon2I).',
  `username` VARCHAR(255) UNIQUE COMMENT 'Nome de usuário.',
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB COMMENT 'Usuário do sistema.';

CREATE TABLE IF NOT EXISTS `Products` (
  `_id` CHAR(36) BINARY  COMMENT 'Código de identificação (UUIDv4).',
  `bar_code` VARCHAR(255) COMMENT 'Código de barras.',
  `category` INTEGER COMMENT 'Categoria.',
  `in_stock` INTEGER COMMENT 'Quantidade.',
  `name` VARCHAR(255) COMMENT 'Nome.',
  `product_picture` VARCHAR(255) UNIQUE COMMENT 'Foto.',
  `user_id` CHAR(36) BINARY COMMENT 'Código de identificação do usuário (UUIDv4).',
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  PRIMARY KEY (`_id`),
  FOREIGN KEY (`user_id`) REFERENCES `Users` (`_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT 'Produto do sistema.';
